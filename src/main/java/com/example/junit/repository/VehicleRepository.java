package com.example.junit.repository;

import com.example.junit.entity.Vehicle;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, String> {

   public default List<Vehicle> getAllvehicle(){
        List<Vehicle> response = new ArrayList<>();
        response.add(new Vehicle("1","Test","Model123",2020,true));
        response.add(new Vehicle("1","Test","Model123",2020,true));
        response.add(new Vehicle("1","Test","Model123",2020,true));
        return  response;
    }

}
