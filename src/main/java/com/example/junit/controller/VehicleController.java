package com.example.junit.controller;

import com.example.junit.service.VehicleService;
import com.example.junit.entity.Vehicle;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo")
public class VehicleController {

    @Autowired
    VehicleService vehicleService;

    @PostMapping("/vehicles")
    public ResponseEntity<List<Vehicle>> getAllVehicles() {

        List<Vehicle> vehicles = vehicleService.getAllVehicles();
        if (vehicles.isEmpty()) {
            throw new NullPointerException("No vehicle records were found");
        }
        return new ResponseEntity<List<Vehicle>>(vehicles, HttpStatus.OK);
    }

}
