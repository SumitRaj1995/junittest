package com.example.junit.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import org.springframework.lang.NonNull;

@Entity
public class Vehicle {

    public Vehicle(@NonNull String vin, @NonNull String make, @NonNull String model, @NonNull Integer year,
            Boolean is_older) {
        this.vin = vin;
        this.make = make;
        this.model = model;
        this.year = year;
        this.is_older = is_older;


    }

    @Id
    @Column(name = "VIN", nullable = false, length = 17)
    @NonNull
    private String vin;

    @Column(name = "make", nullable = false)
    @NonNull
    private String make;

    @Column(name = "model", nullable = false)
    @NonNull
    private String model;

    @Column(name = "year", nullable = false)
    @NonNull
    private Integer year;

    @Column(name = "is_older", nullable = true)
    private Boolean is_older;

    @NonNull
    public String getVin() {
        return vin;
    }

    public void setVin(@NonNull String vin) {
        this.vin = vin;
    }

    @NonNull
    public String getMake() {
        return make;
    }

    public void setMake(@NonNull String make) {
        this.make = make;
    }

    @NonNull
    public String getModel() {
        return model;
    }

    public void setModel(@NonNull String model) {
        this.model = model;
    }

    @NonNull
    public Integer getYear() {
        return year;
    }

    public void setYear(@NonNull Integer year) {
        this.year = year;
    }

    public Boolean getIs_older() {
        return is_older;
    }

    public void setIs_older(Boolean is_older) {
        this.is_older = is_older;
    }


}
