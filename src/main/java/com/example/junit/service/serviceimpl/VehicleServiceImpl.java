package com.example.junit.service.serviceimpl;

import com.example.junit.entity.Vehicle;
import com.example.junit.repository.VehicleRepository;
import com.example.junit.service.VehicleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehicleServiceImpl implements VehicleService {

    @Autowired
    VehicleRepository vehicleRepository;

    @Override
    public List<Vehicle> getAllVehicles() {
        return vehicleRepository.getAllvehicle();
    }
}
