package com.example.junit.service.serviceimpl;

import com.example.junit.repository.DataServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class SomeBusinessImpl {

    @Autowired
    private DataServiceRepository dataServiceRepository;

    public int findTheGreatestFromAllData() {
        int[] data = dataServiceRepository.retrieveAllData();
        int greatest = Integer.MIN_VALUE;

        for (int value : data) {
            if (value > greatest) {
                greatest = value;
            }
        }
        return greatest;
    }



}
